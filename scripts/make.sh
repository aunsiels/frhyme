#!/bin/bash

NUM=$1
shift

cat $* | ./truncate.sh $NUM |
  rev | awk --field-separator="\t" '{printf "%s\t%s\n", $2, $1}' |
  ../frhyme/buildtrie.py | ../frhyme/compresstrie.py

